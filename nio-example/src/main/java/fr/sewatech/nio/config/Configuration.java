package fr.sewatech.nio.config;

import fr.sewatech.nio.reporting.ReportConfiguration;

public class Configuration {
    private static final String LOOPS_PROPERTY = "loops";
    private static final String POOL_SIZE_PROPERTY = "poolSize";
    private static final int LOOPS_DEFAULT = 5;
    private static final int POOL_SIZE_DEFAULT = 8;

    private final ReportConfiguration reportConfiguration;
    private final int loops;
    private final int poolSize;

    public static Configuration build() {
        return new Configuration(
                ReportConfiguration.build(),
                Integer.parseInt(System.getProperty(LOOPS_PROPERTY, String.valueOf(LOOPS_DEFAULT))),
                Integer.parseInt(System.getProperty(POOL_SIZE_PROPERTY, String.valueOf(POOL_SIZE_DEFAULT)))
        );
    }

    public Configuration(ReportConfiguration reportConfiguration, int loops, int poolSize) {
        this.reportConfiguration = reportConfiguration;
        this.loops = loops;
        this.poolSize = poolSize;
    }

    public int poolSize() {
        return poolSize;
    }

    public long loops() {
        return loops;
    }

    public ReportConfiguration reportConfiguration() {
        return reportConfiguration;
    }

}
