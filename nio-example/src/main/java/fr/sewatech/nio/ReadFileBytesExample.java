package fr.sewatech.nio;

import fr.sewatech.nio.config.Configuration;
import fr.sewatech.nio.files.IOFunction;
import fr.sewatech.nio.reporting.Report;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.*;
import java.util.stream.Stream;

import static fr.sewatech.nio.files.BigFiles.*;
import static fr.sewatech.nio.reporting.MemoryReport.reportMemoryConfiguration;
import static fr.sewatech.nio.reporting.MemoryReport.reportMemoryUsage;

public class ReadFileBytesExample {

    private final ExecutorService mainExecutor = Executors.newSingleThreadExecutor();
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.HOURS, new ArrayBlockingQueue<>(10));

    private final Configuration configuration;
    private final Report report = new Report();
    private final RunMode runMode;
    private long startTime;

    public ReadFileBytesExample(RunMode runMode) {
        this.runMode = runMode;

        this.configuration = Configuration.build();

        this.executor.setMaximumPoolSize(configuration.poolSize());
        this.executor.setCorePoolSize(configuration.poolSize());

        this.report.putMemoryConfiguration(reportMemoryConfiguration())
                .putLoops(configuration.loops())
                .putRunMode(runMode.name());
    }

    public static void main(String[] args) {
        new ReadFileBytesExample(RunMode.init(args)).run();
    }

    public void run() {
        initBigFiles();

        startTime = System.currentTimeMillis();
        mainExecutor.execute(() -> this.readBigFiles(0));
    }

    private void readBigFiles(int count) {
        if (count < MAX_FILES * configuration.loops()) {
            readBigFile(count, () -> readBigFiles(count + 1));
        } else {
            shutdown();
        }
    }

    private void readBigFile(int count, Runnable callback) {
        executor.execute(() -> {
            try {
                byte[] result = runMode.function.apply(getPath("-" + count % MAX_FILES));
                report.putSomeBytes(extractSomeBytes(result));

                mainExecutor.execute(callback);
            } catch (Throwable e) {
                e.printStackTrace();
                shutdown();
            }
        });
    }

    private void shutdown() {
        report.putMemoryUsage(reportMemoryUsage());
        report.putDuration(System.currentTimeMillis() - startTime);

        report.print(configuration.reportConfiguration());
        mainExecutor.shutdown();
        executor.shutdown();
    }


    private enum RunMode {
        READ_ALL_BYTES(Files::readAllBytes),
        INPUT_STREAM(ReadFileBytesActions::inputStream),
        FILE_CHANNEL(ReadFileBytesActions::fileChannel),
        ASYNC_FILE_CHANNEL(ReadFileBytesActions::asyncFileChannel),
        RANDOM_ACCESS(ReadFileBytesActions::randomAccess),
        MAPPED_BUFFER(ReadFileBytesActions::mappedBuffer);

        private final IOFunction<Path, byte[]> function;

        RunMode(IOFunction<Path, byte[]> ioFunction) {
            this.function = ioFunction;
        }

        static RunMode init(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(READ_ALL_BYTES);
        }
    }
}