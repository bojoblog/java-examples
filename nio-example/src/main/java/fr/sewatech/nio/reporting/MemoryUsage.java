package fr.sewatech.nio.reporting;

public class MemoryUsage {
    private final long directMemoryUsed;
    private final long heapUsed;

    MemoryUsage(long directMemoryUsed, long heapUsed) {
        this.directMemoryUsed = directMemoryUsed;
        this.heapUsed = heapUsed;
    }

    public long directMemoryUsed() {
        return directMemoryUsed;
    }

    public long heapUsed() {
        return heapUsed;
    }
}
