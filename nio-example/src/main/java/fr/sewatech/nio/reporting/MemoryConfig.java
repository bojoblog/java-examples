package fr.sewatech.nio.reporting;

public class MemoryConfig {
    private final String maxDirectMemorySize;
    private final String maxHeapSize;

    MemoryConfig(String maxDirectMemorySize, String maxHeapSize) {
        this.maxDirectMemorySize = maxDirectMemorySize;
        this.maxHeapSize = maxHeapSize;
    }

    public String maxDirectMemorySize() {
        return maxDirectMemorySize;
    }

    public String maxHeapSize() {
        return maxHeapSize;
    }
}
