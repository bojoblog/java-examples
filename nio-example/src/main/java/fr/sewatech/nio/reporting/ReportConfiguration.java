package fr.sewatech.nio.reporting;

public class ReportConfiguration {
    private static final String TEMPLATE_PROPERTY = "report.template";
    private static final String ATTRIBUTES_PROPERTY = "report.attributes";
    private static final String TEMPLATE_DEFAULT = "%s : %s ms";
    private static final String ATTRIBUTES_DEFAULT = "RunMode,Duration";

    private final String template;
    private final String attributes;

    public static ReportConfiguration build() {
        return new ReportConfiguration(
                System.getProperty(TEMPLATE_PROPERTY, TEMPLATE_DEFAULT),
                System.getProperty(ATTRIBUTES_PROPERTY, ATTRIBUTES_DEFAULT)
        );
    }

    public ReportConfiguration(String template, String attributes) {
        this.template = template;
        this.attributes = attributes;
    }

    public String template() {
        return template;
    }

    public String attributes() {
        return attributes;
    }
}
