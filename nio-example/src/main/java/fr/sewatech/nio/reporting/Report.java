package fr.sewatech.nio.reporting;

import fr.sewatech.nio.ReadFileBytesExample;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static fr.sewatech.nio.files.BigFiles.MAX_FILES;
import static fr.sewatech.nio.reporting.MemoryReport.asMB;

public class Report {

    private final Map<String, String> data = new HashMap<>();

    public Report() {
        this.put("MaxFiles", MAX_FILES);
    }

    public void print(ReportConfiguration reportConfiguration) {
        String template = reportConfiguration.template();

        String[] attrs = reportConfiguration.attributes().split(",");
        Object[] values = Arrays.stream(attrs)
                .map(data::get)
                .toArray(String[]::new);
        System.out.printf(template + '\n', values);
    }

    public Report putMemoryConfiguration(MemoryConfig memoryConfig) {
        this.put("MaxDirectMemorySize", asMB(memoryConfig.maxDirectMemorySize()));
        this.put("MaxHeapSize", asMB(memoryConfig.maxHeapSize()));
        return this;
    }

    public Report putLoops(long loops) {
        this.put("Loops", loops);
        return this;
    }

    public void putRunMode(String runMode) {
        this.put("RunMode", runMode);
    }
    public void putMemoryUsage(MemoryUsage memoryUsage) {
        this.put("DirectMemoryUsed", asMB(memoryUsage.directMemoryUsed()));
        this.put("HeapUsed", asMB(memoryUsage.heapUsed()));
    }

    public void putSomeBytes(String bytes) {
        this.put("SomeBytes", bytes);
    }

    public void putDuration(long duration) {
        this.put("Duration", duration);
    }

    private void put(String key, Object value){
        data.put(key, String.valueOf(value));
    }
}
