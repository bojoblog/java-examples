package fr.sewatech.nio.reporting;

import com.sun.management.HotSpotDiagnosticMXBean;

import java.lang.management.BufferPoolMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

public interface MemoryReport {

    static MemoryUsage reportMemoryUsage() {
        System.gc();
        BufferPoolMXBean direct = ManagementFactory
                .getPlatformMXBeans(BufferPoolMXBean.class)
                .stream()
                .filter(pool -> "direct".equals(pool.getName()))
                .findFirst().orElseThrow();
        MemoryMXBean memory = ManagementFactory.getPlatformMXBean(MemoryMXBean.class);

        return new MemoryUsage(direct.getMemoryUsed(), memory.getHeapMemoryUsage().getUsed());
    }

    static MemoryConfig reportMemoryConfiguration() {
        HotSpotDiagnosticMXBean hotSpotDiagnosticMXBean = ManagementFactory.getPlatformMXBean(HotSpotDiagnosticMXBean.class);
        return new MemoryConfig(
                hotSpotDiagnosticMXBean.getVMOption("MaxDirectMemorySize").getValue(),
                hotSpotDiagnosticMXBean.getVMOption("MaxHeapSize").getValue()
        );
    }

    static long asMB(long value) {
        return value / 1024 / 1024;
    }

    static long asMB(String value) {
        return asMB(Long.parseLong(value));
    }
}
