package fr.sewatech.nio;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;

import static java.lang.Integer.min;

public interface ReadFileBytesActions {

    static byte[] inputStream(Path path) throws IOException {
        try (InputStream inputStream = Files.newInputStream(path)) {
            return inputStream.readAllBytes();
        }
    }

    static byte[] fileChannel(Path path) throws IOException {
        int size = (int) path.toFile().length();
        int bufferSize = 64 * 1024;

        try (FileChannel channel = FileChannel.open(path)) {
            byte[] result = new byte[size];

            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

            int position = 0;
            while (position < size) {
                channel.read(buffer, position);
                buffer.flip();
                System.arraycopy(buffer.array(), 0, result, position, min(bufferSize, size - position));
                position += bufferSize;
            }
            return result;
        }
    }

    static byte[] asyncFileChannel(Path path) throws IOException {
        int size = (int) path.toFile().length();
        int bufferSize = 64 * 1024;

        try (AsynchronousFileChannel channel = AsynchronousFileChannel.open(path)) {
            byte[] result = new byte[size];

            int position = 0;
            CountDownLatch countDownLatch = new CountDownLatch((int) Math.ceil(1d * size / bufferSize));
            while (position < size) {
                ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
                int pos = position;
                channel.read(buffer, position, null,
                        new CompletionHandler<Integer, Void>() {
                            @Override
                            public void completed(Integer length, Void attachment) {
                                countDownLatch.countDown();
                                buffer.flip();
                                System.arraycopy(buffer.array(), 0, result, pos, min(bufferSize, size - pos));
                            }

                            @Override
                            public void failed(Throwable e, Void attachment) {
                                countDownLatch.countDown();
                                System.err.println(e);
                            }
                        });
                position += bufferSize;
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            return result;
        }
    }

    static byte[] randomAccess(Path path) throws IOException {
        int size = (int) path.toFile().length();
        try (RandomAccessFile file = new RandomAccessFile(path.toFile(), "r")) {
            byte[] result = new byte[size];
            file.read(result);
            return result;
        }
    }

    static byte[] mappedBuffer(Path path) throws IOException {
        int size = (int) path.toFile().length();

        try (FileChannel channel = FileChannel.open(path)) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);

            byte[] result = new byte[size];
            buffer.get(result);
            return result;
        }
    }
}