package fr.sewatech.nio.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.stream.IntStream;

public interface BigFiles {

    String BIG_FILE_NAME = "big-file%s.bin";
    int MAX_FILES = 10;
    int FILE_SIZE_MB = 500;

    static void initBigFiles() {
        int size = FILE_SIZE_MB * 1024 * 1024;
        IntStream.range(0, MAX_FILES)
                .forEach(value -> writeBigFile((size + size * value / MAX_FILES) / 2, "-" + value));
    }

    static void writeBigFile(int size, String suffix) {
        Path path = getPath(suffix);
        if (Files.exists(path)) {
            return;
        }

        byte[] content = buildRandomByteArray(size);
        try {
            Files.write(path, content);
            System.out.printf("%s written, with last bytes %s \n", path.getFileName(), extractSomeBytes(content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static byte[] buildRandomByteArray(int size) {
        byte[] content = new byte[size];
        new Random().nextBytes(content);
        return content;
    }

    static Path getPath(String suffix) {
        return Path.of(System.getProperty("java.io.tmpdir"), String.format(BIG_FILE_NAME, suffix));
    }

    static String extractSomeBytes(byte[] content) {
        return String.format("[%s, %s, %s, ..., %s, %s, %s]",
                content[0], content[1], content[2],
                content[content.length - 3], content[content.length - 2], content[content.length - 1]);
    }
}
