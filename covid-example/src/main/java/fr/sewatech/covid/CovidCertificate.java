package fr.sewatech.covid;

import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.model.Array;
import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.Map;
import co.nstant.in.cbor.model.NegativeInteger;
import co.nstant.in.cbor.model.UnicodeString;
import co.nstant.in.cbor.model.UnsignedInteger;

public class CovidCertificate {
  public final String lastname;
  public final String firstname;
  public final String birthdate;
  public final String country;
  public final String doseNumber;
  public final String maxDoseNumber;
  public final String date;
  public final String issuer;  // CNAM ou DGS
  public final String manufacturer; // ORG-100030215 => BioNTech Manufacturing GmbH
  public final String medicinalProduct; // EU/1/20/1528 = Comirnaty
  public final String target; // 840539006 => COVID-19
  public final String vaccineProphylaxis; //  J07BX03 => Covid-19 vaccines (ATC code)

  private CovidCertificate(
      String lastname,
      String firstname,
      String birthdate,
      String country,
      String doseNumber,
      String maxDoseNumber,
      String date,
      String issuer,
      String manufacturer,
      String medicinalProduct,
      String target,
      String vaccineProphylaxis) {
    this.lastname = lastname;
    this.firstname = firstname;
    this.birthdate = birthdate;
    this.country = country;
    this.doseNumber = doseNumber;
    this.date = date;
    this.issuer = issuer;
    this.manufacturer = manufacturer;
    this.medicinalProduct = medicinalProduct;
    this.maxDoseNumber = maxDoseNumber;
    this.target = target;
    this.vaccineProphylaxis = vaccineProphylaxis;
  }

  @Override
  public String toString() {
    return String.format(
        "Nom de famille : %s%n" +
        "Prénom : %s%n" +
        "Date de naissance : %s%n" +
        "Maladie ou agent ciblé : %s - %s%n" +
        "Vaccin/prophylaxie : %s - %s%n" +
        "Médicament vaccinal : %s - %s%n" +
        "Fabricant : %s - %s%n" +
        "Nombre de doses : %s/%s%n" +
        "Date de vaccination : %s%n" +
        "État membre de vaccination : %s%n" +
        "Émetteur du certificat : %s%n",
        lastname, firstname, birthdate,
        Reference.labelOfTarget(target), target,
        Reference.labelOfProphylaxis(vaccineProphylaxis), vaccineProphylaxis,
        Reference.labelOfMedicinalProduct(medicinalProduct), medicinalProduct,
        Reference.labelOfManufacturer(manufacturer), manufacturer,
        doseNumber, maxDoseNumber, date, country, issuer);
  }

  public static CovidCertificate fromCbor(DataItem cbor) throws CborException {
    Map bodyItem = (Map) CborDecoder.decode(
        ((ByteString) ((Array) (cbor)).getDataItems().get(2)).getBytes()).get(0);
    Map mainItem = (Map) ((Map) bodyItem
        .get(new NegativeInteger(-260)))
        .get(new UnsignedInteger(1));
    Map nameItem = (Map) mainItem.get(new UnicodeString("nam"));
    Map vaccineItem = (Map) ((Array) mainItem.get(new UnicodeString("v"))).getDataItems().get(0);

    return new CovidCertificate(
        nameItem.get(new UnicodeString("fn")).toString(),
        nameItem.get(new UnicodeString("gn")).toString(),
        mainItem.get(new UnicodeString("dob")).toString(),
        vaccineItem.get(new UnicodeString("co")).toString(),
        vaccineItem.get(new UnicodeString("dn")).toString(),
        vaccineItem.get(new UnicodeString("sd")).toString(),
        vaccineItem.get(new UnicodeString("dt")).toString(),
        vaccineItem.get(new UnicodeString("is")).toString(),
        vaccineItem.get(new UnicodeString("ma")).toString(),
        vaccineItem.get(new UnicodeString("mp")).toString(),
        vaccineItem.get(new UnicodeString("tg")).toString(),
        vaccineItem.get(new UnicodeString("vp")).toString());
  }
}
