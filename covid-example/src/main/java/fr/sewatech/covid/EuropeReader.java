package fr.sewatech.covid;

import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.model.Array;
import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.Map;
import co.nstant.in.cbor.model.NegativeInteger;
import com.jcraft.jzlib.InflaterInputStream;
import nl.minvws.encoding.Base45;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class EuropeReader {

  public static void main(String[] args) throws IOException, CborException {
    String base45Content = new QRCodeReader().read(new File(args[0]));

    byte[] compressedContent = Base45.getDecoder()
        .decode(base45Content.replace("HC1:", ""));

    List<DataItem> cbor = new CborDecoder(new InflaterInputStream(new ByteArrayInputStream(compressedContent)))
        .decode();
    cbor.stream()
//        .map(DataItem::getTag)
        .forEach(System.out::println);

    CovidCertificate covidCertificate = CovidCertificate.fromCbor(cbor.get(0));
//    System.out.println(covidCertificate);
  }

}
