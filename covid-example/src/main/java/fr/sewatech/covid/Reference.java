package fr.sewatech.covid;

import java.util.Map;

public class Reference {
  private static final Map<String, String> TARGETS = Map.of("840539006", "COVID-19");
  private static final Map<String, String> PROPHYLAXES = Map.of("J07BX03", "Covid-19 vaccines");
  private static final Map<String, String> MEDICINAL_PRODUCTS = Map.of(
      "EU/1/20/1528", "Comirnaty",  // See https://ec.europa.eu/health/documents/community-register/html/h1528.htm
      "EU/1/20/1525", "COVID-19 Vaccine Janssen", // See https://ec.europa.eu/health/documents/community-register/html/h1525.htm
      "EU/1/20/1507", "Spikevax", // See https://ec.europa.eu/health/documents/community-register/html/h1507.htm
      "EU/1/21/1529", "Vaxzevria" // see https://ec.europa.eu/health/documents/community-register/html/h1529.htm
  );
  private static final Map<String, String> MANUFACTURERS = Map.of( // See https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-certificates_dt-specifications_en.pdf
      "ORG-100001699", "AstraZeneca AB",
      "ORG-100030215", "Biontech Manufacturing GmbH",
      "ORG-100001417", "Janssen-Cilag International",
      "ORG-100031184", "Moderna Biotech Spain S.L.",
      "ORG-100006270", "Curevac AG",
      "ORG-100013793", "CanSino Biologics",
      "ORG-100020693", "China Sinopharm International Corp. - Beijing location",
      "ORG-100010771", "Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location",
      "ORG-100024420", "Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location",
      "ORG-100032020", "Novavax CZ AS"
  );

  private static final String UNKNOWN = "UNKNOWN";

  public static String labelOfTarget(String key) {
    return TARGETS.getOrDefault(key, UNKNOWN);
  }

  public static String labelOfProphylaxis(String key) {
    return PROPHYLAXES.getOrDefault(key, UNKNOWN);
  }

  public static String labelOfMedicinalProduct(String key) {
    return MEDICINAL_PRODUCTS.getOrDefault(key, UNKNOWN);
  }

  public static String labelOfManufacturer(String key) {
    return MANUFACTURERS.getOrDefault(key, UNKNOWN);
  }
}
