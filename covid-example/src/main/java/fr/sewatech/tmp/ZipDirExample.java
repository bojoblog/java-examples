package fr.sewatech.tmp;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

public class ZipDirExample {

  public static void main(String[] args)  {
    var stackTraceException = new StackTraceException();
    System.out.println(getStackTraceAsString(stackTraceException));

    Thread.dumpStack();
    System.out.println(Arrays.toString(Thread.currentThread().getStackTrace()));
  }

  private static class StackTraceException extends Throwable {
  }

  public static String getStackTraceAsString (Throwable throwable) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    throwable.printStackTrace(pw);
    return sw.getBuffer().toString();
  }
}
