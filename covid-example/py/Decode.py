import sys
import zlib
import pprint
# Modules tiers (Pillow, Pyzbar, base45, cbor2)
import PIL.Image
import pyzbar.pyzbar
import base45
import cbor2

img = PIL.Image.open("/home/alexis/Misc/hubic/Perso/papiers-alexis/covid/qrCode1.jpeg")
data = pyzbar.pyzbar.decode(img)
cert = data[0].data.decode()
b45data = cert.replace("HC1:", "")
zlibdata = base45.b45decode(b45data)
cbordata = zlib.decompress(zlibdata)
decoded = cbor2.loads(cbordata)
# print("Header\n----------------")
# pprint.pprint(cbor2.loads(decoded.value[0]))
print("\nPayload\n----------------")
pprint.pprint(cbor2.loads(decoded.value[2]))
# print("\nSignature ?\n----------------")
# print(decoded.value[3])
