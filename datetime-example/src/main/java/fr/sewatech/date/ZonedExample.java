package fr.sewatech.date;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;

public class ZonedExample {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
  private static final ZoneId UTC = ZoneId.of("UTC");
  private static final ZoneId PARIS = ZoneId.of("Europe/Paris");

  public static void main(String[] args) {
    doItForZone(UTC);
    System.out.println();
    doItForZone(PARIS);
  }

  private static void doItForZone(ZoneId zone) {
    Instant instantBeforeWinterTime = Instant.parse("2000-10-29T00:00:00Z");
    ZonedDateTime beforeWinterTime = ZonedDateTime.ofInstant(instantBeforeWinterTime, zone);
    print(beforeWinterTime);

    printPlusOne(beforeWinterTime, ChronoUnit.DAYS);
    printPlusOne(beforeWinterTime, ChronoUnit.WEEKS);
    printPlusOne(beforeWinterTime, ChronoUnit.MONTHS);
  }

  private static void printPlusOne(ZonedDateTime dateTime, ChronoUnit unit) {
    System.out.println("=== " + unit);
    print(dateTime.plus(1, unit));
  }

  private static void print(ZonedDateTime dateTime) {
    System.out.printf("%s - %s%n", ZonedExample.FORMATTER.format(dateTime), dateTime.toInstant());
  }

}
