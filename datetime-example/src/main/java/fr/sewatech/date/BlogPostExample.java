package fr.sewatech.date;

import org.threeten.extra.chrono.DiscordianDate;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.Chronology;
import java.time.chrono.HijrahDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;

public class BlogPostExample {

  public static void main(String[] args) {
    System.out.println(LocalDate.now());
    System.out.println(HijrahDate.now());
    System.out.println("====================");

    Chronology.getAvailableChronologies()
        .stream()
        .sorted(Comparator.comparing(chrono -> chrono.getClass().getCanonicalName()))
        .map(Chronology::dateNow)
        .forEach(System.out::println);
    System.out.println("====================");

    LocalDate isoStart = LocalDate.parse("2000-03-01");
    System.out.println(ChronoUnit.DAYS.between(isoStart, isoStart.plus(1, ChronoUnit.MONTHS)));
    System.out.println(ChronoUnit.DAYS.between(isoStart, isoStart.plus(1, ChronoUnit.YEARS)));
    HijrahDate hijrahStart = HijrahDate.from(isoStart);
    System.out.println(ChronoUnit.DAYS.between(hijrahStart, hijrahStart.plus(1, ChronoUnit.MONTHS)));
    System.out.println(ChronoUnit.DAYS.between(hijrahStart, hijrahStart.plus(1, ChronoUnit.YEARS)));

    System.out.println(ChronoUnit.DAYS.between(isoStart, isoStart.plus(1, ChronoUnit.WEEKS)));
    DiscordianDate discordianStart = DiscordianDate.from(isoStart);
    System.out.println(ChronoUnit.DAYS.between(discordianStart, discordianStart.plus(1, ChronoUnit.WEEKS)));

    try {
      System.out.println(Instant.EPOCH.plus(7, ChronoUnit.DAYS));
      System.out.println(Instant.EPOCH.plus(1, ChronoUnit.WEEKS));
    } catch (java.time.temporal.UnsupportedTemporalTypeException e) {
      System.out.println("Erreur: " + e);
    }

    System.out.println(Instant.EPOCH.atZone(ZoneId.of("UTC")).plus(1, ChronoUnit.WEEKS).toInstant());

    System.out.println("====================");
    Instant instantBeforeWinterTime = Instant.parse("2000-10-29T00:00:00Z");
    System.out.println(ChronoUnit.HOURS.between(instantBeforeWinterTime, instantBeforeWinterTime.plus(1, ChronoUnit.DAYS)));

    ZonedDateTime beforeWinterTimeUtc = ZonedDateTime.ofInstant(instantBeforeWinterTime, ZoneId.of("UTC"));
    System.out.println(ChronoUnit.HOURS.between(beforeWinterTimeUtc, beforeWinterTimeUtc.plus(1, ChronoUnit.DAYS)));

    ZonedDateTime beforeWinterTimeParis = ZonedDateTime.ofInstant(instantBeforeWinterTime, ZoneId.of("Europe/Paris"));
    System.out.println(ChronoUnit.HOURS.between(beforeWinterTimeParis, beforeWinterTimeParis.plus(1, ChronoUnit.DAYS)));
  }

}
