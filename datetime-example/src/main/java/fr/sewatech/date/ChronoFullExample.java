package fr.sewatech.date;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.chrono.ChronoZonedDateTime;
import java.time.chrono.Chronology;
import java.time.chrono.IsoChronology;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ChronoFullExample {

  private static final ZoneId UTC = ZoneId.of("UTC");
  private static LocalDate baseDate;
  private static Map<ChronoUnit, Long> isoDurations;
  private static final List<ChronoUnit> UNITS = List.of(
      ChronoUnit.WEEKS,
      ChronoUnit.MONTHS,
      ChronoUnit.YEARS
  );

  public static void main(String[] args) {
    baseDate = LocalDate.parse("2000-01-01");
    isoDurations = UNITS.stream()
        .collect(Collectors.toMap(unit -> unit, unit -> computeDurationOfUnit(IsoChronology.INSTANCE, unit)));

    Chronology.getAvailableChronologies()
        .stream()
        .sorted(Comparator.comparing(chrono -> chrono.getClass().getCanonicalName()))
        .filter(chrono -> chrono != IsoChronology.INSTANCE)
        .forEach(ChronoFullExample::detectNonIsoDurations);
  }

  private static void detectNonIsoDurations(Chronology chronology) {
    String messages = ChronoFullExample.UNITS.stream()
        .map(unit -> detectNonIsoDuration(chronology, unit))
        .filter(Objects::nonNull)
        .collect(Collectors.joining("\n"));
    if (!messages.isEmpty()) {
      System.out.println("=== " + chronology);
      System.out.println(messages);
    }
  }

  private static String detectNonIsoDuration(Chronology chronology, ChronoUnit unit) {
    Long duration = computeDurationOfUnit(chronology, unit);
    return duration.equals(isoDurations.get(unit)) ? null : String.format("%s: %s days", unit, duration);
  }

  private static Long computeDurationOfUnit(Chronology chronology, ChronoUnit unit) {
    ChronoZonedDateTime<?> before = ChronoZonedDateTime.from(
        chronology.date(baseDate).atTime(LocalTime.MIDNIGHT).atZone(UTC));
    return ChronoUnit.DAYS.between(before, before.plus(1, unit));
  }

}
