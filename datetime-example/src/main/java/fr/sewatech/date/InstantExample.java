package fr.sewatech.date;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;

public class InstantExample {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
  private static final ZoneId ZONE = ZoneId.of("Europe/Paris");

  public static void main(String[] args) {
    Instant beforeWinterTime = ZonedDateTime.of(2000, 10, 29, 2, 30, 0, 0, ZONE)
        .toInstant();
    print(beforeWinterTime);

    printPlusOne(beforeWinterTime, ChronoUnit.HOURS);
    printPlusOne(beforeWinterTime, ChronoUnit.DAYS);
    printPlusOne(beforeWinterTime, ChronoUnit.WEEKS);
    printPlusOne(beforeWinterTime, ChronoUnit.MONTHS);
  }

  private static void printPlusOne(Instant instant, ChronoUnit unit) {
    System.out.println("=== " + unit);
    try {
      print(instant.plus(1, unit));
    } catch (UnsupportedTemporalTypeException e) {
      System.out.println("\u26A0 " + e.getMessage());
    }
  }

  private static void print(Instant instant) {
    System.out.printf("%s - %s%n", InstantExample.FORMATTER.format(instant.atZone(ZONE)), instant);
  }

}
