package fr.sewatech.signal;

import sun.misc.Signal;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.DAYS;

public class Main {
  public static void main(String[] args) {
    handleShutdown();
    handleSignal(SIG.INT);
    handleSignal(SIG.HUP);
//    handleSignal(SIG.KILL);
  }

  private static void handleShutdown() {
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread(() -> System.out.println("Cleaning before shutdown"))
        );
  }

  private static void handleSignal(SIG sig) {
    new Thread(sig::handle)
        .start();
  }

  public enum SIG {
    HUP("hangup"),
    INT("interrupt"),
    QUIT("quit"),
    ABRT("abort"),
    KILL("kill"),
    TERM("terminate");

    final String description;
    final Signal signal;

    SIG(String description) {
      this.description = description;
      this.signal = new Signal(this.name());
    }

    void handle() {
      Signal.handle(
          this.signal,
          signal -> System.out.printf("Received %s - %s : %s%n", signal.getName(), signal.getNumber(), this.description));
      try {
        Thread.sleep(DAYS.toMillis(1));
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
  }
}
