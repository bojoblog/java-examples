package fr.sewatech.cert;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Reader for X.509 certificates
 */
public class X509CertificateReader {

    private final CertificateFactory certificateFactory;

    public X509CertificateReader() {
        try {
            certificateFactory = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            throw new RuntimeCertificateException(e);
        }
    }

    /**
     * Read certificate in a PEM-formatted file
     */
    public X509Certificate readPEM(String certificateFileName) {
        try {
            InputStream is = Files.newInputStream(Path.of(certificateFileName));
            return (X509Certificate) certificateFactory.generateCertificate(is);
        } catch (CertificateException | IOException e) {
            throw new RuntimeCertificateException(e);
        }
    }

}
