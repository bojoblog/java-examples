package fr.sewatech.cert;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Reader for PKCS#7 certificates
 */
public class Pkcs7CertificateReader {

    private final JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter().setProvider("BC");

    public Pkcs7CertificateReader() {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Read first certificate in a PKCS#7 DER-formatted file
     */
    public X509Certificate readDER(String certificateFileName) {
        try {
            InputStream is = Files.newInputStream(Path.of(certificateFileName));
            X509CertificateHolder certificateHolder = new CMSSignedData(is)
                    .getCertificates()
                    .getMatches(null)
                    .stream().findFirst().orElseThrow();
            return certificateConverter.getCertificate(certificateHolder);
        } catch (CMSException | CertificateException | IOException e) {
            throw new RuntimeCertificateException(e);
        }
    }

}
