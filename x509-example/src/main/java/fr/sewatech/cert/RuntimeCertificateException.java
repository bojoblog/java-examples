package fr.sewatech.cert;

public class RuntimeCertificateException extends RuntimeException {

    public RuntimeCertificateException(Exception e) {
        super(e);
    }

}
