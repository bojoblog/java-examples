package fr.sewatech.cert;

import javax.security.auth.x500.X500Principal;
import java.security.Principal;
import java.security.cert.X509Certificate;

public class X509Example {

    private static final X509CertificateReader x509CertificateReader = new X509CertificateReader();;
    private static final Pkcs7CertificateReader pkcs7CertificateReader = new Pkcs7CertificateReader();

    public static void main(String[] args) {
        // Prepare certificates:
        /*
            openssl req -x509 -sha256 -nodes -newkey rsa:2024 -subj "/O=Sewatech/OU=Blog/C=FR" -keyout cert.key -out cert/cert.pem
            openssl crl2pkcs7 -nocrl -certfile cert/cert.pem -outform DER -out cert/cert.p7b
         */
        X509Certificate x509Certificate = x509CertificateReader.readPEM("cert/cert.pem");
        print(x509Certificate);

        System.out.println("----------------");

        X509Certificate pkcs7Certificate = pkcs7CertificateReader.readDER("cert/cert.p7b");
        print(pkcs7Certificate);
    }

    private static void print(X509Certificate x509Certificate) {
        Principal subjectDN = x509Certificate.getSubjectDN();
        System.out.println("SubjectDN:            " + subjectDN);
        System.out.println("  Class :             " + subjectDN.getClass());
        System.out.println("  Name :              " + subjectDN.getName()); // Same as toString()
        X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
        System.out.println("SubjectX500Principal: " + subjectX500Principal); // Same as RFC2253
        System.out.println("  Class :             " + subjectX500Principal.getClass());
        System.out.println("  Name :              " + subjectX500Principal.getName()); // Same as RFC2253
        System.out.println("  Name (RFC2253) :    " + subjectX500Principal.getName(X500Principal.RFC2253));
        System.out.println("  Name (RFC1779) :    " + subjectX500Principal.getName(X500Principal.RFC1779));
        System.out.println("  Name (CANONICAL) :  " + subjectX500Principal.getName(X500Principal.CANONICAL));
    }

}
