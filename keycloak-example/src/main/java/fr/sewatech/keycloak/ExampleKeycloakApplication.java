package fr.sewatech.keycloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleKeycloakApplication {

	// https://docs.spring.io/spring-security/site/docs/current/reference/html5/#oauth2
	public static void main(String[] args) {
		SpringApplication.run(ExampleKeycloakApplication.class, args);
	}

}
