package fr.sewatech.keycloak;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping()
public class MainResource {

    @GetMapping("/echo/{who}")
    public ResponseEntity<String> hello(@PathVariable String who) {
        return ResponseEntity.ok("Hello " + who + " from " + getUsername());
    }

    private String getUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof Jwt) {
            // Resource server
            return ((Jwt)principal).getClaimAsString("preferred_username");
        } else if (principal instanceof DefaultOidcUser) {
            // Client login
            return ((DefaultOidcUser)principal).getPreferredUsername();
        } else if (principal instanceof UserDetails) {
            // Basic / form authentication
            return ((UserDetails) principal).getUsername();
        } else {
            return "unknown";
        }
    }

}
