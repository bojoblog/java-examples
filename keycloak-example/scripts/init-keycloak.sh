base_url=http://localhost:8888

# Authenticate and get token
token=$(curl --silent \
             --data "client_id=admin-cli" --data "username=admin" --data "password=admin" --data "grant_type=password" \
             "$base_url/auth/realms/master/protocol/openid-connect/token" \
          | jq -r .access_token)

# Create realm
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
     --data "{\"realm\":\"example-realm\", \"enabled\":true}" \
     $base_url/auth/admin/realms

# Create client with secret
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json"  \
     --data "{\"clientId\":\"example-client\", \"enabled\":true, \"standardFlowEnabled\":true,  \
          \"directAccessGrantsEnabled\":true, \"rootUrl\":\"http://localhost:8181\",  \
          \"redirectUris\":[\"http://localhost:8181/*\"], \"secret\":\"example-secret\"}"  \
     $base_url/auth/admin/realms/example-realm/clients

# Create user with password
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json"  \
     --data "{\"username\":\"swuser\", \"enabled\":true,  \
          \"credentials\": [{\"type\":\"password\",\"value\":\"swpwd\",\"temporary\":\"false\"}]}"  \
     $base_url/auth/admin/realms/example-realm/users

# Create user without password
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json"  \
     --data "{\"username\":\"swx509user\", \"enabled\":true}"  \
     $base_url/auth/admin/realms/example-realm/users

# Create user with Serial and IssuerDN
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json"  \
     --data "{\"username\":\"swserialuser\", \"enabled\":true, \
              \"attributes\":{\"SerialNumber\":[\"2\"],\"IssuerDN\":[\"o=sewatech,cn=client-ca\"]}}"  \
     $base_url/auth/admin/realms/example-realm/users

# X509 direct flow with "Certificate Serial Number and IssuerDN"
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
     --data "{\"alias\":\"x509 direct grant\", \"providerId\": \"basic-flow\", \"topLevel\": true, \
          \"authenticationExecutions\": [{\"authenticator\": \"direct-grant-auth-x509-username\", \"requirement\": \"REQUIRED\", \"priority\": 0, \"userSetupAllowed\": false, \"autheticatorFlow\": false}]}"  \
     $base_url/auth/admin/realms/example-realm/authentication/flows
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
      --data "{\"provider\":\"direct-grant-auth-x509-username\"}" \
     $base_url/auth/admin/realms/example-realm/authentication/flows/x509%20direct%20grant/executions/execution
curl -X PUT --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
     --data "{\"directGrantFlow\": \"x509 direct grant\"}" \
     $base_url/auth/admin/realms/example-realm
execution_id=$(curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
                    $base_url/auth/admin/realms/example-realm/authentication/flows/x509%20direct%20grant/executions | jq -r .[0].id)
curl --silent --header "Authorization: bearer $token" --header "Content-Type: application/json" \
     --data "{\"config\": {\"x509-cert-auth.mapping-source-selection\":\"Certificate Serial Number and IssuerDN\", \
                           \"x509-cert-auth.mapper-selection\":\"Custom Attribute Mapper\",
                           \"x509-cert-auth.mapper-selection.user-attribute-name\":\"SerialNumber##IssuerDN\", \
                           \"x509-cert-auth.canonical-dn-enabled\":\"true\", \
                           \"x509-cert-auth.user-attribute-name\":\"usercertificate\", \
                           \"x509-cert-auth.confirmation-page-disallowed\":\"true\"}, \
              \"alias\":\"X509serial\"}" \
     $base_url/auth/admin/realms/example-realm/authentication/executions/$execution_id/config
