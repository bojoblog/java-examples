docker kill kc-example || true
docker run --publish 8888:8080 --publish 8883:8443 \
           --env KEYCLOAK_USER=admin --env KEYCLOAK_PASSWORD=admin \
           --env X509_CA_BUNDLE=/etc/x509/https/ca-client.bundle \
           --volume $(pwd)/keycloak-tls:/etc/x509/https \
           --name kc-example --rm \
           --detach jboss/keycloak:12.0.4
#           --detach quay.io/keycloak/keycloak:12.0.4